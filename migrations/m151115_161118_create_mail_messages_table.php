<?php

use yii\db\Migration;

class m151115_161118_create_mail_messages_table extends Migration
{
    public function up()
    {
        $this->createTable('mail_message', [
            'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
            'email' => 'VARCHAR(200) NOT NULL',
            'subject' => 'VARCHAR(500) NOT NULL',
            'content' => 'TEXT',
            'priority' => 'TINYINT(3) UNSIGNED NOT NULL DEFAULT 0',
            'isVisible' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',
            'isSent' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
            'isFailed' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
            'failReason' => 'VARCHAR(200) DEFAULT NULL',
            'attempts' => 'TINYINT(3) UNSIGNED NOT NULL DEFAULT 0',
            'timeCreated' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'timeUpdated' => 'TIMESTAMP NULL DEFAULT NULL',
            'PRIMARY KEY (`id`)'
        ]);
    }

    public function down()
    {
        $this->dropTable('mail_message');
    }
}
