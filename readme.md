
Database spool for Yii 2 SwiftMailer Extension
===============================

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist b52by/yii2-swiftmailer-database-spool
```

or add

```json
"b52by/yii2-swiftmailer-database-spool": "~1.0"
```

to the require section of your composer.json.

Migrations
----------

Run

```
php yii migrate --migrationPath=@vendor/b52by/yii2-swiftmailer-database-spool/migrations
```