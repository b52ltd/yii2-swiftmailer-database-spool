<?php
namespace B52by\Yii2\SwiftMailer;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @author Artem Belov <razor2909@gmail.com>
 *
 * @property integer $id
 * @property string  $email
 * @property string  $subject
 * @property string  $content
 * @property string  $priority
 * @property boolean $isVisible
 * @property boolean $isSent
 * @property boolean $isFailed
 * @property string  $failReason
 * @property integer $attempts
 * @property string  $timeCreated
 * @property string  $timeUpdated
 */
class Message extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{mail_message}}';
    }

    /**
     * @return MessageQuery
     */
    public static function find()
    {
        return new MessageQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'timeCreated',
                'updatedAtAttribute' => 'timeUpdated',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'string'],
            ['email', 'trim'],

            ['subject', 'string'],
            ['subject', 'trim'],

            ['content', 'string'],
            ['content', 'trim'],

            ['priority', 'integer'],
            ['priority', 'default', 'value' => 1],
        ];
    }

    /**
     * @return array
     */
    public function safeAttributes()
    {
        return ['email', 'subject', 'content', 'priority'];
    }
}
