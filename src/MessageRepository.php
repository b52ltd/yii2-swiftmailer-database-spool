<?php
namespace B52by\Yii2\SwiftMailer;

use yii\base\InvalidValueException;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MessageRepository
{
    /**
     * @return Message
     */
    public function pop()
    {
        $message = null;

        Message::getDb()->transaction(function() use (&$message) {
            /* @var $message Message */
            if ($message = Message::find()->visible()->prioritized()->old()->one()) {
                $this->markInvisible($message);
                $this->incrementAttempts($message);
            }
        });

        return $message;
    }

    /**
     * @param array $attributes
     * @return Message
     */
    public function create($attributes)
    {
        $message = new Message();
        $message->setAttributes($attributes);
        $this->saveOrFail($message);

        return $message;
    }

    /**
     * @param Message $message
     */
    public function incrementAttempts($message)
    {
        $message->updateCounters(['attempts' => 1]);
    }

    /**
     * @param Message $message
     */
    public function markVisible($message)
    {
        $message->isVisible = 1;
        $this->saveOrFail($message);
    }

    /**
     * @param integer[] $primaryKeys
     */
    public function markAllVisible($primaryKeys)
    {
        $timeUpdated = date('Y-m-d H:i:s');
        Message::updateAll(['isVisible' => 1, 'timeUpdated' => $timeUpdated], ['id' => $primaryKeys]);
    }

    /**
     * @param Message $message
     */
    public function markInvisible($message)
    {
        $message->isVisible = 0;
        $this->saveOrFail($message);
    }

    /**
     * @param Message $message
     */
    public function markSucceed($message)
    {
        $message->isSent = 1;
        $message->content = '';
        $this->saveOrFail($message);
    }

    /**
     * @param Message $message
     * @param string $reason
     */
    public function markFailed($message, $reason = null)
    {
        $message->isFailed = 1;
        $message->failReason = $reason;
        $this->saveOrFail($message);
    }

    /**
     * @param Message $message
     */
    protected function saveOrFail($message)
    {
        if ( ! $message->save()) {
            throw new InvalidValueException('Failed to save the message into database.');
        }
    }
}
