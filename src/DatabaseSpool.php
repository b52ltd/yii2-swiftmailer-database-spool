<?php
namespace B52by\Yii2\SwiftMailer;

use Swift_Mime_Message;
use Swift_Transport;
use yii\helpers\ArrayHelper;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class DatabaseSpool extends \Swift_ConfigurableSpool
{
    /**
     * @var MessageRepository
     */
    protected $messages;


    /**
     * @param MessageRepository $messages
     */
    public function __construct(MessageRepository $messages)
    {
        $this->messages = $messages;
    }

    /**
     * Starts this Spool mechanism.
     */
    public function start()
    {
    }

    /**
     * Stops this Spool mechanism.
     */
    public function stop()
    {
    }

    /**
     * Tests if this Spool mechanism has started.
     *
     * @return boolean
     */
    public function isStarted()
    {
        return true;
    }

    /**
     * Queues a message.
     *
     * @param Swift_Mime_Message $message The message to store
     * @return bool Whether the operation has succeeded
     */
    public function queueMessage(Swift_Mime_Message $message)
    {
        $this->messages->create([
            'email' => implode(',', array_keys($message->getTo())),
            'subject' => $message->getSubject(),
            'content' => $this->serializeSwiftMessage($message),
        ]);
    }

    /**
     * Sends messages using the given transport instance.
     *
     * @param Swift_Transport $transport A transport instance
     * @param string[]        $failedRecipients An array of failures by-reference
     * @return int The number of sent emails
     */
    public function flushQueue(Swift_Transport $transport, &$failedRecipients = null)
    {
        $messagesCount = 0;

        while (true) {
            $queuedMessage = $this->messages->pop();
            if (!$queuedMessage) {
                break;
            }

            try {
                $swiftMessage = $this->unserializeSwiftMessage($queuedMessage->content);
                $count = $this->sendSwiftMessage($transport, $swiftMessage, $failedRecipients);

                if ($count) {
                    $this->messages->markSucceed($queuedMessage);
                    $messagesCount += $count;
                }
            } catch (\Exception $e) {
                $this->messages->markFailed($queuedMessage, $e->getMessage());
            }
        }

        return $messagesCount;
    }

    /**
     * Execute a recovery if for any reason a process is sending for too long.
     *
     * @param integer $timeout in second Defaults is for very slow smtp responses
     * @param integer $attempts to send failed message
     */
    public function recover($timeout = 900, $attempts = 3)
    {
        $recoveryDate = date('Y-m-d H:i:s', time() - $timeout);

        $query = Message::find()->invisible()->notSent()->notFailed($attempts)->notModifiedSince($recoveryDate);

        foreach ($query->batch() as $messages) {
            $primaryKeys = ArrayHelper::getColumn($messages, 'id');
            $this->messages->markAllVisible($primaryKeys);
        }
    }

    /**
     * @param Swift_Mime_Message $message
     * @return string
     */
    protected function serializeSwiftMessage(Swift_Mime_Message $message)
    {
        return serialize($message);
    }

    /**
     * @param string $content
     * @throws Exception
     * @return Swift_Mime_Message
     */
    protected function unserializeSwiftMessage($content)
    {
        $message = @unserialize($content);

        if ($message instanceof Swift_Mime_Message) {
            return $message;
        }

        throw new Exception('Unable to unserialize swiftmailer message.');
    }

    /**
     * @param Swift_Transport $transport
     * @param Swift_Mime_Message $message
     * @param string[] $failedRecipients
     * @return integer number of recipients who were accepted for delivery.
     */
    protected function sendSwiftMessage(Swift_Transport $transport, Swift_Mime_Message $message, &$failedRecipients = null)
    {
        if ( ! $transport->isStarted()) {
            $transport->start();
        }

        return $transport->send($message, $failedRecipients);
    }
}
