<?php
namespace B52by\Yii2\SwiftMailer;

use yii\db\ActiveQuery;

/**
 * @author Artem Belov <razor2909@gmail.com>
 */
class MessageQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function old()
    {
        return $this->orderBy('timeCreated ASC');
    }

    /**
     * @return static
     */
    public function last()
    {
        return $this->orderBy('timeUpdated ASC');
    }

    /**
     * @return static
     */
    public function prioritized()
    {
        return $this->orderBy('priority DESC');
    }

    /**
     * @return static
     */
    public function visible()
    {
        return $this->andWhere(['isVisible' => 1]);
    }

    /**
     * @return static
     */
    public function invisible()
    {
        return $this->andWhere(['isVisible' => 0]);
    }

    /**
     * @return static
     */
    public function failed()
    {
        return $this->andWhere(['isFailed' => 1]);
    }

    /**
     * @param integer $attempts
     * @return static
     */
    public function notFailed($attempts = null)
    {
        if ($attempts === null) {
            return $this->andWhere(['isFailed' => 0]);
        }

        return $this->andWhere('isFailed = 0 OR (isFailed = 1 AND attempts < :attempts)', [':attempts' => $attempts]);
    }

    /**
     * @return static
     */
    public function sent()
    {
        return $this->andWhere(['isSent' => 1]);
    }

    /**
     * @return static
     */
    public function notSent()
    {
        return $this->andWhere(['isSent' => 0]);
    }

    /**
     * @param string $date
     * @return static
     */
    public function notModifiedSince($date)
    {
        return $this->andWhere(['<', 'timeUpdated', $date]);
    }
}
